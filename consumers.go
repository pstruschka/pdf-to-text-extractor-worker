package main

import (
	"encoding/json"
	"github.com/minio/minio-go"
)

var FILENAME = "pdfs.tgz"

func task() {
	for d := range tasks {
		logger.Printf("Recieved msg: %s", d.Body)

		task := Task{}
		err := json.Unmarshal(d.Body, &task)
		logOnError(err, "failed to decode task")
		if err != nil {
			continue
		}

		tasksChan <- task
	}
}

func doTask() {
	for task := range tasksChan {
		logger.Printf("bucket: %s, object: %s", task.Bucket, task.Object)
		err := minioClient.FGetObject(task.Bucket, task.Object, FILENAME, minio.GetObjectOptions{})
		logOnError(err, "failed to get object")
		if err != nil {
			rabbit.PublishObject(Status{
				Bucket:  task.Bucket,
				Status:  "error",
				Message: err.Error(),
			}, statusQueue, false)
			continue
		}
		err = extractArchive(task.Bucket, task.Object, FILENAME)
		logOnError(err, "failed to extract archive")
		if err != nil {
			rabbit.PublishObject(Status{
				Bucket:  task.Bucket,
				Status:  "error",
				Message: err.Error(),
			}, statusQueue, false)
			continue
		}
		logger.Printf("extracted")
	}
}