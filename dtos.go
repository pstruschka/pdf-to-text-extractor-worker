package main

type Task struct {
	Bucket string `bson:"bucket",json:"bucket"`
	Object string `bson:"object",json:"object"`
}

type Status struct {
	Bucket  string   `json:"bucket"`
	Status  string   `json:"status"`
	Total   int      `json:"total,omitempty"`
	Targets []string `json:"targets,omitempty"`
	Message string   `json:"msg,omitempty"`
}

type Compress struct {
	Bucket             string   `json:"bucket"`
	IntermediateBucket string   `json:"intermediate_bucket"`
	Object             string   `json:"object"`
	Targets            []string `json:"targets"`
}

type ErrorMessage struct {
	Message string `bson:"message",json:"message"`
}
