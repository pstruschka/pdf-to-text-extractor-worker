package main

import (
	"archive/tar"
	"compress/gzip"
	"container/list"
	"errors"
	"fmt"
	"github.com/minio/minio-go"
	"github.com/twinj/uuid"
	"io"
	"os"
	"path"
	"strings"
)

func extractArchive(bucket string, object string, fileName string) error {

	file, err := os.Open(fileName)
	logOnError(err, "failed to open file")
	if err != nil {
		return err
	}
	defer file.Close()

	zr, err := gzip.NewReader(file)
	logOnError(err, "failed to decompress gzip")
	if err != nil {
		return err
	}

	intermediateBucket := fmt.Sprintf("%s-%s", bucket, uuid.NewV4().String())
	err = minioClient.MakeBucket(intermediateBucket, "")
	logOnError(err, "failed to create bucket")

	objects := list.List{}

	tr := tar.NewReader(zr)
	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			break // End of archive
		}
		logOnError(err, "failed tar")
		if err != nil {
			return err
		}

		/* Check that the structure is flat */
		if path.Dir(hdr.Name) != "." {
			logger.Printf("Warning: not flat structure")
			return errors.New(fmt.Sprintf("%s is not a flat structure", object))
		}

		/* OSx populates tar with ._ metadata, ignore them */
		if strings.HasPrefix(path.Base(hdr.Name), "._") {
			continue
		}

		/* Check that file has .pdf extension */
		if path.Ext(hdr.Name) != ".pdf" {
			logger.Printf("Warning: not pdf")
			return errors.New(fmt.Sprintf("%s is not a pdf", hdr.Name))
		}

		baseName := strings.TrimSuffix(hdr.Name, ".pdf")
		objects.PushBack(baseName + ".txt")

		n, err := minioClient.PutObject(intermediateBucket, hdr.Name, tr, hdr.Size, minio.PutObjectOptions{ContentType: "application/pdf"})
		logOnError(err, "failed to upload object")
		logger.Printf("Wrote %d of %d to minio %s/%s", n, hdr.Size, intermediateBucket, hdr.Name)

		task := Task{
			Bucket: intermediateBucket,
			Object: hdr.Name,
		}

		rabbit.PublishObject(task, workerQueue, true)
	}
	targets := make([]string, 0, objects.Len())
	for e := objects.Front(); e != nil; e = e.Next() {
		targets = append(targets, e.Value.(string))
	}
	rabbit.PublishObject(Status{
		Bucket:  bucket,
		Status:  "extracted",
		Total:   objects.Len(),
		Targets: targets,
	}, statusQueue, false)
	rabbit.PublishObject(Compress{
		Bucket:             bucket,
		IntermediateBucket: intermediateBucket,
		Object:             fmt.Sprintf("txt-%s", object),
		Targets:            targets,
	}, compressorQueue, false)

	return nil
}
