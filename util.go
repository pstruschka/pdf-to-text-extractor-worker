package main

import (
	"fmt"
	"os"
)

func getEnv(key string, def string) string {
	val, ok := os.LookupEnv(key)
	if !ok {
		return def
	} else {
		return val
	}
}

func failOnError(err error, msg string) {
	if err != nil {
		logger.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func logOnError(err error, msg string) {
	if err != nil {
		logger.Printf("%s: %s", msg, err)
	}
}

func notifyOnError(err error, msg string) {
	if err != nil {
		logger.Printf("%s: %s", msg, err)
		errorMessage := ErrorMessage{
			Message: msg,
		}
		rabbit.PublishObject(&errorMessage, statusQueue, false)
	}
}