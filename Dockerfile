FROM golang:1.11-alpine

RUN apk update && \
    apk upgrade && \
    apk add git

ADD https://raw.githubusercontent.com/eficode/wait-for/master/wait-for /usr/local/bin/
RUN chmod +x /usr/local/bin/wait-for

WORKDIR /go/src/pdf-to-text-extractor-worker
COPY *.go ./

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["pdf-to-text-extractor-worker"]
