package main

import (
	"encoding/json"
	"net/http"
)

func PostJob(w http.ResponseWriter, r *http.Request) {
	task := Task{}

	err := json.NewDecoder(r.Body).Decode(&task)
	logOnError(err, "failed to decode task")
	if err != nil {
		w.WriteHeader(400)
		return
	}
	logger.Printf("send task")
	tasksChan <- task


	//rabbit.PublishObject(&task, "")
	w.WriteHeader(200)
}

func StaticServer(w http.ResponseWriter, r *http.Request) {
	logger.Printf("GET STATIC %s", r.URL.Path)
	http.ServeFile(w, r, r.URL.Path)
}
