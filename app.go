package main

import (
	"fmt"
	"github.com/minio/minio-go"
	"github.com/streadway/amqp"
	"log"
	"net/http"
	"os"
)

var (
	logger     = log.New(os.Stderr, "", log.Lshortfile)
	rabbit     *RabbitConnector
	rabbitHost = getEnv("RABBIT_HOST", "127.0.0.1")
	rabbitURL  = fmt.Sprintf("amqp://guest:guest@%s:5672/", rabbitHost)
	taskQueue = getEnv("TASK_QUEUE", "extract")
	statusQueue = getEnv("STATUS_QUEUE", "update_status")
	workerQueue = getEnv("WORKER_QUEUE", "worker")
	compressorQueue = getEnv("COMPRESS_QUEUE", "compress")

	minioEndpoint              = getEnv("MINIO_ENDPOINT", "127.0.0.1:9000")
	minioAccessKey             = getEnv("MINIO_ACCESS_KEY", "")
	minioSecretKey             = getEnv("MINIO_SECRET_KEY", "")
	minioClient *minio.Client

	port                    = ":" + getEnv("PORT", "8000")
	tasks     <-chan amqp.Delivery
	tasksChan = make(chan Task)
)

func main() {
	rabbit = New(rabbitURL, taskQueue, statusQueue, workerQueue, compressorQueue)
	defer rabbit.Close()

	var err error = nil
	minioClient, err = minio.New(minioEndpoint, minioAccessKey, minioSecretKey, false)
	failOnError(err, "Failed to initialize minio client")

	router := NewRouter()

	tasks, err = rabbit.channel.Consume(taskQueue, "", false, false, false, false, nil)
	logOnError(err, "failed to start consume")

	go doTask()
	go task()

	logger.Printf("START")
	logger.Fatal(http.ListenAndServe(port, router))
}
