package main

import (
	"encoding/json"
	"github.com/streadway/amqp"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type RabbitConnector struct {
	connection *amqp.Connection
	channel    *amqp.Channel
	closeError chan *amqp.Error
	ready chan bool
}

func connectToRabbitMQ(url string) (*amqp.Connection, error) {
	var err error = nil
	for counter := 0; counter < 10; counter++ {
		con, err := amqp.Dial(url)

		if err == nil {
			return con, nil
		}

		logger.Println(err)
		logger.Printf("Trying to reconnect to RabbitMQ at %s\n", url)
		time.Sleep(5 * time.Second)
	}
	return nil, err
}

func (connector *RabbitConnector) rabbitConnector(url string, queues []string) {
	var rabbitErr *amqp.Error

	for {
		rabbitErr = <-connector.closeError
		if rabbitErr != nil {
			logger.Printf("Connecting to %s\n", url)

			var err error
			connector.connection, err = connectToRabbitMQ(url)
			failOnError(err, "Failed to connect to RabbitMQ")

			connector.closeError = make(chan *amqp.Error)
			connector.connection.NotifyClose(connector.closeError)

			ch, err := connector.connection.Channel()
			failOnError(err, "Failed to open channel")
			logger.Printf("RabbitMQ channel Opened")
			connector.channel = ch

			for _, queue := range queues {
				_, err := ch.QueueDeclare(
					queue,
					true,
					false,
					false,
					false,
					nil,
				)
				failOnError(err, "Failed to declare queue")
			}
			connector.ready <- true
		}
	}
}

func New(url string, queue...string) *RabbitConnector {
	connector := new(RabbitConnector)

	connector.closeError = make(chan *amqp.Error)
	connector.ready = make(chan bool)

	go connector.rabbitConnector(url, queue)

	connector.closeError <- amqp.ErrClosed
	ready := <- connector.ready
	if ready {
		return connector
	}
	return nil
}

func mkMarshal(isBinary bool) func(task interface{}) ([]byte, error) {
	if isBinary {
		return bson.Marshal
	} else {
		return json.Marshal
	}
}

func (connector *RabbitConnector) PublishObject(task interface{}, queue string, isBinary bool) {
	marshal := mkMarshal(isBinary)

	data, err := marshal(task)
	if err != nil {
		logOnError(err, "failed to marshal")
		return
	}

	contentType := "application/json"
	if isBinary {
		contentType = "application/bson"
	}

	err = connector.channel.Publish(
		"",
		queue,
		false,
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  contentType,
			Body:         data,
		})
	failOnError(err, "failed to publish a message")
	logger.Printf(" [x] Sent %s", task)
}

func (connector *RabbitConnector) PublishString(message string, queue string) {

	err := connector.channel.Publish(
		"",
		queue,
		false,
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         []byte(message),
		})
	failOnError(err, "Failed to publish a message")
	logger.Printf(" [x] Sent %s", message)
}

func (connector *RabbitConnector) Close() {
	e := connector.channel.Close()
	logOnError(e, "Failed to close channel")
	logger.Printf("RabbitMQ channel closed")
	e = connector.connection.Close()
	logOnError(e, "Failed to close connection")
	logger.Printf("RabbitMQ connection closed")
}
