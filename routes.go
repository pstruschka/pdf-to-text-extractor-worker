package main

import (
	"net/http"
)

type Route struct {
	Prefix      bool
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		false,
		"PostJob",
		"POST",
		"/task",
		PostJob,
	},
	Route{
		true,
		"StaticServer",
		"GET",
		"/static/",
		StaticServer,
	},
}
